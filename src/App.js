import React from 'react'
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom";
import Header from './components/Header'

//Components
import Home from './components/Home'
import Products from './components/Products/Products'
import Contact from './components/Contact'
import Cart from './components/Cart/Cart'
import Collection from './components/Collections/Collection'

export const NameContext = React.createContext()
export const ColorContext = React.createContext()

function App() {
  return (
    <div className="App" >
      <div className="fondo1" />
      <div className="fondo2" />
      <div className="fondo3" />
      <div className="fondo4" />
      <Router>
        <Header />
        <div className="main">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/productos" component={Products} />
            <Route exact path="/contacto" component={Contact} />
            <Route exact path="/carrito" component={Cart} />
            <Route exact path="/colecciones" component={Collection} />
          </Switch>
        </div>
      </Router>
    </div>
  );
}
export default App;
