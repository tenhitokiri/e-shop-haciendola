import { useState, useEffect } from 'react'
import axios from 'axios'
import styled from 'styled-components'

function Search() {
  const [products, setProducts] = useState([])
  const [text, setText] = useState('')
  const [suggestions, setSuggestions] = useState([])

  useEffect(() => {
    const loadProducts = async () => {
      const response = await axios.get('https://staging.haciendola.dev/backend/test-front/api/products/')
      setProducts(response.data)
    }
    loadProducts()
  }, []
  )
  const suggestionHandler = (text) => {
    setText(text)
    setSuggestions([])
  }
  const onChangeHandler = (text) => {
    let matches = []
    if (text.length>0) {
      matches = products.filter(prod => {
        const regex = new RegExp(`${text}`,"gi")
        return prod.title.match(regex)
      })
    }
    setSuggestions(matches)
    setText(text)
  }
  return (
    <SearchBar>
      <input type="text"
        placeholder="Buscar por nombre..."
        style={{ marginLeft: 2 }}
        onChange={e=>onChangeHandler(e.target.value)}
        onBlur={() => { 
          setTimeout(() =>{
            setSuggestions([])
          }, 1000)
        }}
        value={text}
      />
      <button type="submit" style={{ marginLeft: 2 }}>></button>
      {suggestions && suggestions.map((suggestion, i) =>
          <div key={i} 
          onClick={()=>suggestionHandler(suggestion.title)}
          >{suggestion.title}</div>
        )}
    </SearchBar>
  );
}
export default Search;

const SearchBar = styled.div`
  position: relative;
    width: 25vh;
    height: 2vh;
    border-radius: 5%;
    background-color: rgba(124, 154, 165, 0.3);
    z-index: 2;
    input {
        width: 100%;
        height: 2vh;
        border: none;
        background-color: transparent;
        color: #000;
        outline: none;
        &::placeholder {
            color: #000;
        }   
    }
    button {
        position: absolute;
        right: 0;
        top: 0;
        width: 2vh;
        height: 2vh;
        border-radius: 25% 0% 0%  25%;
        background-color: rgba(13, 38, 29, 1);
        border: none;
        outline: none;
        cursor: pointer;
    }
    div {
      cursor: pointer;
      width: 100%;
      border-right: 1px solid black;
      border-left: 1px solid black;
      border-bottom: 1px solid black;
      background-color: rgba(66, 96, 114, 0.7);
      border-radius: 0 0 5% 5%;
      box-shadow: 0px 0px 10px #000;
      font-size: 12px;
      z-index: 1;
      &:hover {
          background-color: rgba(124, 154, 165, 1);
          z-index: 2;
      }
      &:hover div {
          z-index: 2;
      }
    }
`