import React, { useEffect, useState } from 'react'
import axios from 'axios'
import ProductListSlide from './Products/ProductListSlide'

const Home = () => {
    const [bestProducts, setBestProducts] = useState([])
    useEffect(() => {
        (async () => {
            const res = await axios.get('https://staging.haciendola.dev/backend/test-front/api/products/getBestSellers/')
            setBestProducts(res.data)
        })();

    }, [])
    return (
        <div className="ModuleContainer">
            <div className="container container1">
                <div className="Title">
                    <h1>Bienvenido a la Tiendita</h1>
                </div>
                <div>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                </div>
            </div>
            <div className="container container3">
                <div className="Title"><h1>Productos mas Vendidos</h1></div>
                <div className="ProductList">
                    <ProductListSlide productList={bestProducts} />
                </div>
            </div>
        </div>
    )
}
export default Home
