import React from 'react'
import styled from 'styled-components'
import HighlightOffTwoToneIcon from '@material-ui/icons/HighlightOffTwoTone';
import { useState } from 'react'
import { NavLink, Link } from "react-router-dom";
import ShoppingCartTwoToneIcon from '@material-ui/icons/ShoppingCartTwoTone';
import MenuTwoToneIcon from '@material-ui/icons/MenuTwoTone';
import { useSelector } from 'react-redux'

function Header() {
    const items = useSelector(state => state.cart.numberOfItems)
    const [openMenu, setOpenMenu] = useState(false)
    //close menu when clicking on the menu icon
    const handleClickMenu = () => setOpenMenu(!openMenu)

    const itemMarckup = items > 0 ?
        <NavLink exact to="/carrito"><ShoppingCartTwoToneIcon />({items})</NavLink > :
        <NavLink exact to="#"><ShoppingCartTwoToneIcon /></NavLink>

    return (
        <div className="Menu">
            <Link to="/">
                <img src="./images/logotipo.png" alt="logo" />
            </Link>
            <div className="MidMenu">
                <div >
                    <NavLink to="/">Inicio</NavLink>
                </div>
                <div >
                    <NavLink exact to="/productos">Productos</NavLink>
                </div>
                <div >
                    <NavLink exact to="/colecciones">Colecciones</NavLink>
                </div>
                <div >
                    <NavLink exact to="/contacto">Contacto</NavLink>
                </div>
            </div>
            <div className="RightMenu">
                <div>
                    <NavLink to="#">Login</NavLink>
                </div>
                <div>
                    {itemMarckup}
                </div>
                <div className="MenuIconContainer">
                    <NavLink to="#">
                        <MenuTwoToneIcon onClick={handleClickMenu} />
                    </NavLink>
                </div>
            </div>
            <BurgerNav show={openMenu}>
                <div className="CloseContainer">
                    <HighlightOffTwoToneIcon onClick={() => handleClickMenu()} />
                </div>
                <li><NavLink exact to="/">Inicio</NavLink></li>
                <li><NavLink exact to="/productos">Productos</NavLink></li>
                <li><NavLink exact to="/contacto">Contacto</NavLink></li>
                <li><NavLink exact to="/carrito">Carrito</NavLink></li>
                <li><NavLink exact to="/colecciones">Colecciones</NavLink></li>
            </BurgerNav>
        </div>
    )
}
export default Header

const BurgerNav = styled.div`
    position: fixed;
    width: 200px;
    z-index: 10;
    background: rgba(53, 82, 89, 1);
    top: 0;
    right: 0;
    bottom: 0;
    padding: 20px;
    list-style: none;
    align-items: center;
    transform: ${props => props.show ? `translateX(0%)` : `translateX(100%)`}; 
    transition: transform 0.3s ease-in-out;
    li {
        cursor: pointer;
        padding: 12px;
        border-bottom: 1px solid #e0e0e0;
        a{
            font-size: 1rem;
        }
        & hover{
            background: white;
        }
    }
    @media (max-width: 768px) {
        padding-right: 40px;
    }
`