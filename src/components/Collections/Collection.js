import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import Product from '../Products/Product'
import { connect } from 'react-redux'
import { fetchProducts } from '../../redux'

const Collection = ({ fetchProducts, productsData }) => {
    const [products, setProducts] = useState([])
    const [search, setSearch] = useState('')
    useEffect(() => {
        fetchProducts()
    }, []) // eslint-disable-line react-hooks/exhaustive-deps
    useEffect(() => {
        setProducts(productsData.products)
    }
        , [productsData.products]) // eslint-disable-line react-hooks/exhaustive-deps

    const filteredProducts = search.length === 0 ? products :
        products.filter(product => product.title.toLowerCase().includes(search.toLowerCase()))

    const listMarkup = filteredProducts.length > 0 ? (filteredProducts.map(product => (
        <Product key={product.id} product={product} />
    )))
        : (<div className="container">No products found</div>)

    return productsData.loading ? (<h2>Loading...</h2>) : productsData.error ? (<h2>{productsData.error}</h2>) : (
        <Container>
            {
            }
            <SearchBar>
                <h1>Listado de Productos... {filteredProducts.length} encontrados</h1>
                <input
                    type="text"
                    placeholder="Buscar Nombre"
                    value={search}
                    onChange={(e) => setSearch(e.target.value)}
                />
            </SearchBar>

            <ProductList>
                {listMarkup}
            </ProductList>
        </Container>
    )
}

const mapStateToProps = state => ({
    productsData: state.products
})

const mapDispatchToProps = dispatch => ({
    fetchProducts: () => dispatch(fetchProducts())
})
export default connect(mapStateToProps, mapDispatchToProps)(Collection)

const Container = styled.div`
    width: 100%;
    height: 85vh;
    background-color: rgba(124, 154, 165, 0.5);
    display: inherit;
    align-items: center;
    justify-content: center;
    -webkit-flex-direction: column;
    flex-flow: row;
`
const ProductList = styled.div`
    width: 100%;
    height: 88%;
    display: -webkit-box;
    display: flex;
    align-items: center;
    justify-content: center;
    -webkit-flex-direction: column;
    flex-flow: row wrap;
    overflow-x: scroll;
`
const SearchBar = styled.div`
position: relative;
    border-radius: 5%;
    margin-top: 5px;
    margin-bottom: 5px;
    z-index: 2;
    h1 {
        font-size: 1.5em;
        text-align: center;
        color: #fff;
        padding: 10px;
    }
    input {
        width: 95%;
        height: 2vh;
        background-color: rgba(13, 38, 29, 0.4);
        color: #fff;
        padding-left: 5px;
        outline: none;
        &::placeholder {
            color: #fff;
            text-align: center;
        }   
    }
`