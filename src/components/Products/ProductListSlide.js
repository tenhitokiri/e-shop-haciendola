import React from 'react'
import { useState, useEffect } from 'react'
import Product from './Product'

function ProductListSlide({ productList }) {
    const [currentSlide, setCurrentSlide] = useState(0)
    const nextSlide = () => {
        setCurrentSlide(currentSlide === productList.length - 1 ? 0 : currentSlide + 1)
    }
    useEffect(() => {
        setTimeout(nextSlide, 5000)
    })
    return (
        <div className="ProductSlider" id="slider">
            {productList.length > 0 ? (productList.map((product, i) => (
                <div id={product.handle} className={i === currentSlide ? "slide active" : "slide"}>
                    <Product product={product} key={product.id} size="medium" />
                </div>
            )))
                :
                (<h1>No products found</h1>)
            }
        </div>
    )
}
export default ProductListSlide

