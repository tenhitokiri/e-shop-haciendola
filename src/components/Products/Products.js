import React, { useEffect, useState } from 'react'
import Product from './Product'
import { connect } from 'react-redux'
import { fetchProducts } from '../../redux'

const Contact = ({ fetchProducts, productsData }) => {
    const [products, setProducts] = useState([])
    const [search, setSearch] = useState('')
    useEffect(() => {
        fetchProducts()
    }, []) // eslint-disable-line react-hooks/exhaustive-deps
    useEffect(() => {
        setProducts(productsData.products)
    }
        , [productsData.products]) // eslint-disable-line react-hooks/exhaustive-deps

    const filteredProducts = search.length === 0 ? products :
        products.filter(product => product.title.toLowerCase().includes(search.toLowerCase()))

    const listMarkup = filteredProducts.length > 0 ? (filteredProducts.map(product => (
        <Product key={product.id} product={product} />
    )))
        : (<div className="container">No products found</div>)

    return productsData.loading ? (
        <div className="ModuleContainer">
            <h2>Loading...</h2>
        </div>
    ) : productsData.error ? (
        <div className="ModuleContainer">
            <h2>{productsData.error}</h2>
        </div>
    )
        : (
            <div className="ModuleContainer">
                <div className="container container2">
                    <div className="Title">
                        <h1>Listado de Productos... {filteredProducts.length} encontrados</h1>
                    </div>
                    <div className="SearchBar">
                        <input
                            type="text"
                            placeholder="Buscar Nombre"
                            value={search}
                            onChange={(e) => setSearch(e.target.value)}
                        />
                    </div>
                </div>
                <div className="ProductList">
                    {listMarkup}
                </div>
            </div>
        )
}

const mapStateToProps = state => ({
    productsData: state.products
})

const mapDispatchToProps = dispatch => ({
    fetchProducts: () => dispatch(fetchProducts())
})
export default connect(mapStateToProps, mapDispatchToProps)(Contact)
