import React from 'react'
import AddShoppingCartTwoToneIcon from '@material-ui/icons/AddShoppingCartTwoTone';
import AddCircleOutlineTwoToneIcon from '@material-ui/icons/AddCircleOutlineTwoTone';
import RemoveCircleTwoToneIcon from '@material-ui/icons/RemoveCircleTwoTone';
import HighlightOffTwoToneIcon from '@material-ui/icons/HighlightOffTwoTone';
import useCounter from '../../hooks/UseCounter';
import { useDispatch } from 'react-redux'
import { addToCart } from '../../redux'
import { FormatMoney } from 'format-money-js';

function Product({ product, size }) {
    const { handle, title,
        Vendor, type,
        variantSku, variantGrams,
        variantInventoryQty,
        variantPrice, imageSrc
    } = product

    const formatMoney = new FormatMoney({ decimals: 2, symbol: '$', grouping: true })
    const formatPrice = new FormatMoney({ decimals: 2, symbol: ' Grs', append: true, grouping: true })
    const prodPrice = formatMoney.from(parseInt(variantPrice)) || variantPrice
    const prodGrams = formatPrice.from(parseInt(variantGrams)) || variantGrams

    const dispatch = useDispatch()
    let itemsToBuy = 0
    const addCart = () => {
        const payload = {
            handle, title,
            variantInventoryQty,
            variantPrice, imageSrc, itemsToBuy
        }
        dispatch(addToCart(payload))
    }

    const Buttons = ({ initialCount, value, max }) => {
        const [count, increment, decrement, reset] = useCounter(initialCount, value, max)
        const add = () => {
            increment()
            itemsToBuy = count + value > max ? max : count + value
        }
        const remove = () => {
            decrement()
            itemsToBuy = count - value < 0 ? 0 : count - value
        }
        const resetCount = () => {
            reset()
            itemsToBuy = 0
        }
        return (
            <div className="ButtonGroup">
                <button className="Button ButtonRegular" onClick={add} ><AddCircleOutlineTwoToneIcon /></button>
                <div className="Button">
                    <span className="Badge" >{count}</span>
                </div>
                <button className="Button ButtonRegular" onClick={remove} ><RemoveCircleTwoToneIcon /></button>
                <button className="Button ButtonReset" onClick={resetCount} ><HighlightOffTwoToneIcon /></button>
            </div >
        )
    }

    return (
        <div className={size === "medium" ? "Card2 Card2Medium" : "Card2 Card2Small"} key={variantSku} >
            <div className="CardHeader"><h1>{title}</h1></div>
            <div className="CardImage">
                <img src={imageSrc} alt={variantSku} />
            </div>
            <div className="CardBody">
                <p>
                    Fabricante: <span>{Vendor}</span><br />
                    Tipo: <span>{type}</span><br />
                    Peso: <span>{prodGrams}</span><br />
                    Existencia: <span>{variantInventoryQty} Item(s)</span><br />
                </p>
                <div className="CardButton">
                    <div className="CardPrice ">
                        Precio: <span> {prodPrice} </span>
                    </div>
                    <button className="Button ButtonSuccess" onClick={addCart}><AddShoppingCartTwoToneIcon /></button>
                    <Buttons initialCount={0} value={1} max={variantInventoryQty} />
                </div>
            </div>
        </div>
    )
}
export default Product