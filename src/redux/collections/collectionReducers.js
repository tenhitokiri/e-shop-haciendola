import  {FETCH_COLLECTION_REQUEST,
    FETCH_COLLECTION_SUCCESS,
    FETCH_COLLECTION_FAILURE} from './collectionTypes'

const initCollectionsState = {
    loading : false,
    collection : [],
    error : ''
}

const collectionReducer = (state = initCollectionsState, action) => {
    switch(action.type)
    { 
    case FETCH_COLLECTION_REQUEST:
        return {
        ...state,
        loading : true
        }
        case FETCH_COLLECTION_SUCCESS:
        return {
            ...state,
            loading : false,
            collection : action.payload,
            error : ''
        }
        case FETCH_COLLECTION_FAILURE:
        return {
            ...state,
            loading : false,
            collection : [],
            error : action.payload
        }
    default: return state
    }
}

export default collectionReducer