import  {FETCH_COLLECTION_REQUEST,
    FETCH_COLLECTION_SUCCESS,
    FETCH_COLLECTION_FAILURE} from './productTypes'
import axios from 'axios';

export const fetchCollectionsRequest = () => {
    return {
        type : FETCH_COLLECTION_REQUEST
    }
}

export const fetchCollectionsSuccess = (collections) => {
    return {
        type : FETCH_COLLECTION_SUCCESS,
        payload : collections
    }
}

export const fetchCollectionsFailure = (error) => {
    return {
        type : FETCH_COLLECTION_FAILURE,
        payload : error
    }
}

export const fetchCollections = () => {
    return (dispatch) => {
        dispatch(fetchCollectionsRequest())
        axios.get('https://staging.haciendola.dev/backend/test-front/api/collections')
        .then(response => {
            const collections = response.data
            dispatch(fetchCollectionsSuccess(collections))
        })
        .catch(error => {
            const msg = error.message
            dispatch(fetchCollectionsFailure(msg))
        })
    }
}
