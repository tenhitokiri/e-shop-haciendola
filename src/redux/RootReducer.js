import { combineReducers } from 'redux'
import cartReducer from './cart/cartReducer'
import productsReducer from './product/productsReducers'

const allReducers = combineReducers({
    cart: cartReducer,
    products: productsReducer
})

export default allReducers